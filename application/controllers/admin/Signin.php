<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Signin extends CI_Controller {

	public function index()
	{
		$this->load->helper(array('form', 'url'));

		$this->load->library('form_validation');
		$this->form_validation->set_rules('name', '"用户名"', 'trim|required|xss_clean');
		$this->form_validation->set_rules('password', '"密码"', 'trim|required|xss_clean');
		$this->form_validation->set_message('required', '%s不能为空');
		if ($this->form_validation->run() == FALSE)
		{
			$this->load->view('admin/signin');
		}
		else
		{
			$admin = Admin::find_by_name($this->input->post('name'));

			if($admin && sha1($this->input->post('password').$admin->nonce) == $admin->password) {
				$this->session->set_userdata('admin',$admin->to_array());
				redirect(base_url('admin'));
//				print_r($this->session->userdata('admin'));
			} else {
				redirect(base_url('admin/signin'));
			}
		}

	}

} 