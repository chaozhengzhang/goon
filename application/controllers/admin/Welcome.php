<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Welcome extends CI_Controller {

	public function index()
	{
		$admin = $this->session->userdata('admin');
		if(isset($admin)) {
			$this->load->view('admin/index');
		} else {
			redirect(base_url('admin/signin'));
		}
	}
}