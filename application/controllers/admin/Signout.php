<?php
/**
 * Created by PhpStorm.
 * User: zhang.chaozheng
 * Date: 14-2-7
 * Time: 下午4:02
 */

class Signout extends CI_Controller {

	public function index()
	{
		$this->session->unset_userdata('admin');
		$this->session->sess_destroy();
		redirect(base_url('admin'));
	}
} 