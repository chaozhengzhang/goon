<!DOCTYPE html>
<html>

<head>

	<meta charset="utf-8">

	<title>大王官方网站-数据管理中心-登陆</title>

	<link href="<?php echo base_url('assets/css/bootstrap.min.css');?>" rel="stylesheet">

	<style type="text/css">
		body {
			background-color: #f8f8f8;

		}

		* {
			font-size: 12px;
			font-family: 'Microsoft Yahei',helvetica,arial,verdana,sans-serif;
		}
		/* Login Page */

		.login-panel {
			margin-top: 25%;
		}

		.error-message {
			margin-top: 10px;
		}
		.error-message p {
			color: #F00;
		}
	</style>

</head>

<body>

<div class="container">
	<div class="row">
		<div class="col-md-4 col-md-offset-4">
			<div class="login-panel panel panel-default">
				<div class="panel-heading">
					<h3 class="panel-title" style="font-weight: bold;">请登陆</h3>
				</div>
				<div class="panel-body">

					<?php echo form_open(base_url('admin/signin')); ?>
						<fieldset>
							<div class="form-group">
								<input class="form-control" placeholder="用户名" name="name" type="text" autofocus>
							</div>
							<div class="form-group">
								<input class="form-control" placeholder="密码" name="password" type="password" value="">
							</div>
							<input type="submit" class="btn btn-lg btn-success btn-block" value="确定" />
						</fieldset>
					</form>
					<div class="error-message">
						<?php echo validation_errors(); ?>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>

<!-- Core Scripts - Include with every page -->
<script src="<?php echo base_url('assets/js/jquery-1.10.2.js');?>"></script>
<script src="<?php echo base_url('assets/js/bootstrap.min.js');?>"></script>

</body>

</html>
